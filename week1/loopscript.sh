#!/bin/bash
if [ ! "$*" ]; then
	echo "Not enough information was provided."
	exit 1
else
	for arg in "$s@"; do
		if  [ ! -f $arg ]; then
			echo "The file does not exist."
		elif [ -s $arg ]; then
			echo  "inspected by Andrea " >>  $arg
		else
			rm $arg
		fi
	done
fi

